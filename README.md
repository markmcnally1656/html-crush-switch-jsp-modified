HTML Crush Switch
=================

Summary
-------
HTML code crush: turn it on for uploading, turn it off for working. Keep the web small and the code easy on the eyes.

Warning
-------
Take care with PHP and Javascript embedded codes! The crush process COULD mess them up. It is always a good practice to separate the PHP and Javascript code in separate documents, so you can use a specific crusher for each type. In any case, you should ALWAYS test the webpage after crushing it.

How do I switch?
----------------
You can switch the crush ON and OFF with the command palette (by default, cmd+shift+p in OS X and ctrl+shift+p in Windows and Linux). The commands are called with "HTML Crush Switch ON" and "HTML Crush Switch OFF".

What the switch does?
---------------------
###Crush ON:
* HTML Comments removed (<!-- comment -->)
* JS/PHP Multiline comments removed (/* comment */)
* Indentations and trailing spaces removed.
* New line characters removed if they are after...
    - a HTML tag closer (>)
    - a semicolon, for JS and PHP (;)
    - an open or close brace ({})
    - a simple or double quote ('")

###Crush OFF:
* Separates the code into lines:
    - Separates HTML tags.
    - New line after semicolon or braces.
    - Tries to keep HTML singletons in their own line.
    - Tries to keep small tags in one line.
* Reindent the code, using a specific algorithm (default) or using the ST 'reindent' function.


Changelog
---------
###v1.0 - 2012/03/10
Initial version
