import sublime
import sublime_plugin
import re


singletons = ['!DOCTYPE', 'area', 'base', 'basefont', 'br', 'col', 'frame',
              'hr', 'img', 'input', 'isindex', 'link', 'meta', 'param']

settings_file = 'html_crush_switch.sublime-settings'


def crush_on(code):
    """ Turns the crushing on, removing comments, trilling spaces and empty
    lines.

    """

    code = '\n'.join(line.lstrip() for line in code.split('\n'))

    # Remove HTML comments
    while '<!--' in code:
        code = code[:code.find('<!--')] + code[code.find('-->') + 3:]

    # Remove JS/PHP comments
    while '/*' in code:
        code = code[:code.find('/*')] + code[code.find('*/') + 2:]

    code = re.sub('\n{2,}', '\n', code)  # Remove empty lines
    code = code.replace('>\n', '>')
    code = code.replace(';\n', ';')
    code = code.replace('{\n', '{')
    code = code.replace('}\n', '}')
    code = code.replace('"\n', '" ')
    code = code.replace('\'\n', '\' ')

    return code


def crush_off(code):
    """ Turns the crushing off, separating and formatting the code. """

    # Separate singletons in their own line
    code = re.sub(
        '<({0})( [^>]+)?>'.format('|'.join(singletons)), '\g<0>\n', code)
    code = code.replace('><', '>\n<')
    code = code.replace(';', ';\n')
    code = code.replace('{ ', '{\n')
    code = code.replace(' }', '}\n')

    # Opening tags are followed by \n, unless it's a singleline
    code = re.sub('(\n<\/\w+>)([^<\n]+)', '\g<1>\n\g<2>', code)
    # Closing tags are preceded by \n, unless it's a singleline
    code = re.sub('(\n[^<\n]+)(<\/\w+>\n)', '\g<1>\n\g<2>', code)

    return code


def indent(code):
    def is_singleton(line):
        """ Returns True if the line is formed only by a singleton. """
        return re.match('<({0})( [^>]*)?>'.format('|'.join(singletons)), line)

    def is_singleline(line):
        """ Returns True if the tag is opened and close in the same line. """
        if is_singleton(line):
            return True
        return re.match(r'<(\w+) ?.*>.*<\/\1>', line)

    code = code.split('\n')

    indent_level = 0
    for i, line in enumerate(code):
        if (re.match(r'(<\/\w+|\?>)', line) or
           line.endswith('}')):
            indent_level -= 1

        if indent_level:
            indented_line = '    ' * indent_level + line
        else:
            indented_line = line

        if ((re.match(r'<(\?|\w+)', line) and
           not is_singleline(line)) or
           line.endswith('{')):
            indent_level += 1

        code[i] = indented_line

    return '\n'.join(code)


class HtmlCrushSwitchOffCommand(sublime_plugin.TextCommand):
    def run(self, edit, **kwargs):
        settings = sublime.load_settings(settings_file)

        if not settings.has('use_st_indent'):
            settings.set('use_st_indent', False)
            sublime.save_settings(settings_file)

        if 'html' in self.view.scope_name(0):
            r = sublime.Region(0, self.view.size())
            code = self.view.substr(r)

            changed_code = crush_off(code)
            if not settings.get('use_st_indent'):
                changed_code = indent(changed_code)

            self.view.replace(edit, r, changed_code)
            if settings.get('use_st_indent'):
                self.view.run_command('reindent', {'single_line': False})

            sublime.status_message('Code crush OFF. Length from {0} to {1} characters.'.format(len(code), len(changed_code)))
        else:
            sublime.message_dialog('Sorry! You can only switch the crunch when editing HTML code.')


class HtmlCrushSwitchOnCommand(sublime_plugin.TextCommand):
    def run(self, edit, **kwargs):
        # settings = sublime.load_settings(settings_file)

        if 'html' in self.view.scope_name(0):
            r = sublime.Region(0, self.view.size())
            code = self.view.substr(r)

            changed_code = crush_on(code)

            self.view.replace(edit, r, changed_code)

            sublime.status_message('Code crush ON. Length from {0} to {1} characters.'.format(len(code), len(changed_code)))
        else:
            sublime.message_dialog('Sorry! You can only switch the crunch when editing HTML code.')
